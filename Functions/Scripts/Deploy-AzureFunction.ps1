[cmdletbinding()]
Param(

    [Parameter(Mandatory = $True)]
    [validatescript({
        if (Test-Path $_ -PathType Leaf){
            $True
        }
        else{
            Throw "Invalid file path '$_'"
        }
    })]
    [string]
    $PackagePath,

    [Parameter(Mandatory = $True)]
    [ValidateNotNullOrEmpty()]
    [string]
    $FunctionAppName

)

try{
    
    # This gets the app publishsettings credentials which can then be used to deploy the function
    $FunctionApp = Get-AzureWebsite -Name $FunctionAppName
    Write-Verbose -Verbose "Retrieved publish profile credentials for Function App '$FunctionAppName'"

    $FunctionAppUri = "https://{0}.scm.azurewebsites.net/api/zip/site/wwwroot" -f $FunctionAppName
    $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $FunctionApp.PublishingUsername,$FunctionApp.PublishingPassword)))
    $UserAgent = "powershell/1.0"
    $Headers = @{Authorization = "Basic $base64AuthInfo"}

    # Deploy function
    Invoke-RestMethod -Uri $FunctionAppUri -Headers $Headers -Method PUT -InFile $PackagePath -UserAgent $UserAgent
    Write-Verbose -Verbose "Deployed '$PackagePath' to '$FunctionAppName'"

}
catch{
    throw $_
}

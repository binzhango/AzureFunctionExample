# How to deploy Azure functions

---

The purpose of this document is to provide a high level guide on how to provision the required infrastructure resources and deploy an Azure Function based on a c# class library using PowerShell, following principals which align with a typical CI pipeline. 

A sample project is located in the following repository: https://github.com/dougbrw/AzureFunctionExample which contains an arm template, azure function, and relevant deployment scripts.

## Pre-Requisites:
* Visual Studio
* [Visual Studio Tools for Azure Functions](https://aka.ms/azfunctiontools)
* [Azure PowerShell Module](https://docs.microsoft.com/en-us/powershell/azure/install-azurerm-ps?view=azurermps-3.8.0)
* [NuGet](https://dist.nuget.org/index.html)

## Azure Function Files

This sections describes the files which comprise an Azure function

### [host.json](https://github.com/Azure/azure-webjobs-sdk-script/wiki/host.json)
This file contains global configuration options which are shared between all functions on a given function app, including:
* Polling intervals
* Batch sizes
* Logging level
* Timeout: When using a consumption plan there is a maximum run time of 5 minutes before a function will time out.

### [function.json](https://github.com/Azure/azure-webjobs-sdk-script/wiki/function.json)
This defines any function specific configuration, including:
* Path to the script file or class library
* Input trigger
* Output bindings
* Enabled or disabled state

### Script file
The following languages are supported:
* Bash (.sh)
* Batch (.bat)
* C# script file (.csx)
* C# class library (.dll)
* F# (.fsx)
* JavaScript (.js)
* Php 
* PowerShell (.ps1)
* Python (.py)

### Step 1. Provision Infrastructure ([ARM Template](https://github.com/dougbrw/AzureFunctionExample/blob/master/ProvisioningTemplates/Templates/DeploymentTemplate.json))

A function requires the following resources which should be provisioned using an ARM template:

* **[App Service Hosting Plan](https://docs.microsoft.com/en-us/azure/azure-functions/functions-scale)**: This is essentially a container for the function app which defines the region and pricing tier. The most important choice here is between the "Consumption" or "Dedicated" service plan, as this **cannot** be changed once created. 
* **Storage Account**: A storage account is required for a number of reasons, including logging, and allowing the script runtime to share files when scaling across multiple instances. You can use this same storage account within your function for data storage if required.
* **Function App**: Multiple functions can be deployed to a single function app. Whilst multiple languages can share a single function app, they must all use the same version of the runtime host. This also provides collective management tools (diagnostics, monitoring, etc).

Once the template has finished deploying then continue to the next step.

![ResourceGroup](https://gist.github.com/dougbrw/73dac88472a74fd7abd4ae2b229c41b6/raw/90805b1daa2a239235b9942729061c6abefe07d6/ResourceGroup.PNG)

## Step 2. Build the function class library project

Build **AzureFunctions.sln** using visual studio fom the example repository to proceed to complete this step.
![Solution](https://gist.github.com/dougbrw/73dac88472a74fd7abd4ae2b229c41b6/raw/2a47804a7547f9faf23d3f7144fd363baec457fe/Solution.PNG)

Microsoft have confirmed that the next version of the Visual Studio tooling for Azure Functions will be aimed at using pre-compiled c# class libraries rather than script files. Some advantages when compared to script files are:
* IntelliSense
* [Local debugging](https://github.com/lindydonna/FunctionsAsWebProject#local-debugging-in-visual-studio)
* Unit testing
* Improved performance
* Better continuous integration tooling support

When creating a function make sure the scriptFile property within **function.json** file references the correct path! This must be the relative path to the script file on the remote Azure file system once deployed.
```json
{
  "disabled": false,
  "scriptFile": ".\\bin\\FunctionsLibraryProject.dll",
  "entryPoint": "FunctionsLibraryProject.HelloHttpTrigger.Run",
  "bindings": [
    {
      "authLevel": "function",
      "name": "req",
      "type": "httpTrigger",
      "direction": "in"
    },
    {
      "name": "res",
      "type": "http",
      "direction": "out"
    }
  ]
}
  ```

## Step 3. Create NuGet deployment package

This step would typically be performed by your CI server, but for the purpose of this guide you can create the package using the following command: 
```batchfile
nuget pack AzureFunctions.nuspec -version 1.0.0.0
```

The directory structure of the package content is key, Each function should have its own directory containing the function.json and a sub directory named bin containing the code.

![Nupkg](https://gist.github.com/dougbrw/73dac88472a74fd7abd4ae2b229c41b6/raw/524906af646ee3993d46e9cbaade3d5bb1456642/nupkg.PNG)

If you wish to include a host.json file, then it should be added to the root of the nupkg file. It can be deployed at the same time as a function, or on its own as a seperate step.

## Step 4. Deploy NuGet deployment package ([Deploy-AzureFunction.ps1](https://github.com/dougbrw/AzureFunctionExample/blob/master/Functions/Scripts/Deploy-AzureFunction.ps1))

```PowerShell
 .\Deploy-AzureFunction.ps1 -PackagePath "C:\AzureFunctionExample\FunctionsAsWebProject.1.0.0.0.nupkg" -FunctionAppName "FunctionApp01"
 ```


This step takes the package previously created, and then uses [Azure kudu REST API](https://github.com/projectkudu/kudu/wiki/REST-API)  zip endpoint (*A nupkg file is really just a zip file with some metadata*) to upload the package to the Function App. When successful  the function will be displayed in the portal within a couple of seconds.

If your function is not visible in the portal then now is the time to jump to the Kudu diagnostics debug console, and review the contents of the **site/wwwroot** directory. The function definition file should be located in **site/wwwroot/`<FunctionName`>/function.json** and the class library within **site/wwwroot/`<FunctionName`>/bin**.

The diagnostic console can be found at: **https://`<FunctionAppName`>.scm.azurewebsites.net/DebugConsole**

If you require to perform any configuration transform / substitution at deploy time then you may require some additional steps here:
1. Extract the nupkg contents
2. Perform modifications
3. Re-package into a nupkg file (or create a zip file)
4. Run deployment script using the new package
